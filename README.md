Nowadays, the use of maps for geolocation, directions or other purposes has become trivially 
easy. Using mapping applications, such as Google Maps, has become a routine for most of us. 
To move away from this excessive usage, this project has taken on the challenge of creating a 
mapping application using the highest level of professional software and data in this area. This 
was made possible with the help of Mapbox, a provider of custom online maps for websites, 
and data taken from Ordnance Survey, the official mapping agency of the United Kingdom, in 
order to break away from the use of Google Maps. Ordnance Survey records and keeps 500 
million geospatial features in its master map which is maintained up-to-date. MapOS will serve 
as a platform to display the usefulness of the Ordnance Survey geospatial data and as a small 
tutorial on how to create a mapping application using Django and React.
