from django.urls import path
from .views import index 

app_name = 'frontend'

urlpatterns = [
    path('',index, name=''),
    path('InfoMap', index),
    path('OSRivers', index),
    path('DemoMap', index),
    path('OSMap', index),
    path('ChooseMap', index),

]