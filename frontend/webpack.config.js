const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "./static/frontend"),
    filename: "[name].js",
    sourceMapFilename: "[name].js.map",
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },
  optimization: {
    minimize: true,
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        // This has effect on the react lib size
        NODE_ENV: JSON.stringify("production"),
        REACT_APP_MAPBOX_TOKEN: JSON.stringify("pk.eyJ1IjoiZGllZ29ndWF5YXMiLCJhIjoiY2ttNWR5Nnd1MGRtdDJvbHB3ZjNtZ3YzZiJ9.2NAYxvzu2Rqa25EKMtDriA")
      },
    }),
  ],
};