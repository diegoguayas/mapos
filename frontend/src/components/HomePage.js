import React, { Component } from "react";
import { Grid, Button, ButtonGroup, Typography } from "@material-ui/core";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";
import InfoMap from "./InfoMap";
import OSRivers from "./OSRivers";
import DemoMap from "./DemoMap";
import ChooseMap from "./ChooseMap";
import OSMap from "./OSMap";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Container from '@material-ui/core/Container';

export default class HomePage extends Component {

  renderHomePage() {

    return (
      <div>
        <div >
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              MapOS
            </Typography>
            <Typography variant="h6" align="justify" color="textSecondary" paragraph>
              This web application will make use of Ordnance Survey (OS) maps and tools to display information
              and prove that it is possible to break the consistent use of Google Maps. Ordnance Survey
              are in charge of creating, maintaining and distributing detailed location information for Great Britain.
              They record and keep 500 million geospatial features in the OS master map up-to-date.
            </Typography>
          </Container>
        </div>

      <Container maxWidth="md">

      <Grid container spacing={4}>
          <Grid xs={6} sm={3} md={3}>

                  <Card>
                    <CardMedia
                      image={DemoJPG}
                      title="Demo Map"
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                      Demo Ordnance Survey Map
                      </Typography>
                      <Typography>
                      Demo that shows capabilities of OS maps in contrast to a regular online maps
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button size="small" variant="contained" color="primary" to="/DemoMap" component={Link}>
                        View Map
                      </Button>
                    </CardActions>
                  </Card>
         </Grid>

        <Grid xs={6} sm={3} md={3}>

        <Card>
          <CardMedia
            image={InfoMapa}
            title="Information Map"
          />
          <CardContent >
            <Typography gutterBottom variant="h5" component="h2">
            Information Map
            </Typography>
            <Typography>
            OS map with information of Manchester including a combination of external data taken from
            the Manchester City Council Open Data and OS data. 
            </Typography>
          </CardContent>
          <CardActions>
          <Button size="small" variant="contained" color="primary" to="/InfoMap" component={Link}>
          View Map
          </Button>
          </CardActions>
        </Card>
        </Grid>

         <Grid xs={6} sm={3} md={3}>
        <Card>
          <CardMedia
            image={MapN}
            title="OS Map"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
            Ordnance Survey Map
            </Typography>
            <Typography>
            OS map with similar attributes to Google Maps. This map has road networks, buildings, natural features, points of interest and more.
            </Typography>
          </CardContent>
          <CardActions>
          <Button size="small" variant="contained" color="primary" to="/OSMap" component={Link}>
          View Map
          </Button>
          </CardActions>
        </Card>
        </Grid>

        <Grid xs={6} sm={3} md={3}>

        <Card>
          <CardMedia
            image="https://www.ordnancesurvey.co.uk/image-library/products/open-rivers-detail.x8c811353.jpg?q=100&w=1000"
            title="OS Rivers data"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
            Ordnance Survey Open Rivers
            </Typography>
            <Typography>
            OS Open Rivers GIS data contains over 144,000 km of water bodies and watercourses map data.
            These include freshwater rivers, tidal estuaries and canals. 
            </Typography>
          </CardContent>
          <CardActions>
          <Button size="small" variant="contained" color="primary" to="/OSRivers" component={Link}>
          View Map
          </Button>
          </CardActions>
        </Card>
        </Grid>




       </Grid>
       </Container>

      </div>

    );
  }


  render() {
    return (
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={() => {
              return  this.renderHomePage(); 
            }}
          />
          <Route path="/InfoMap" component={InfoMap} />
          <Route path="/OSRivers" component={OSRivers} />
          <Route path="/DemoMap" component={DemoMap} />
          <Route path="/OSMap" component={OSMap} />
          <Route path="/ChooseMap" component={ChooseMap} />


        </Switch>
      </Router>
    );
  }
}
