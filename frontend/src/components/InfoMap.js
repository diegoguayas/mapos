import * as React from 'react';
import { useState, useEffect } from 'react';
import ReactMapGL, {Marker, Popup, GeolocateControl, NavigationControl} from 'react-map-gl';
import { Button } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import { makeStyles } from '@material-ui/core/styles';
import { Link} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
}));

export default function InfoMap() {
  const [viewport, setViewport] = useState({
    width: "100vw",
    height: "100vh",
    latitude:  53.483959,
    longitude: -2.244644,
    zoom: 12
  });
  const classes = useStyles();

  // const [mapStyle, setMapStyle] = useState('');


  //JSON Tree file for MapWeb -- null request
  const[treeDataAQ, setTreeDataAQ] = useState(null);
  //JSON Tree file for MapWeb
  const[jsonTree, setJsonTree] = useState(null);
  //JSON Public toilet -- null request
  const[publicToiletAQ, setPublicToiletAQ] = useState(null);
  //JSON Public toilet
  const[jsonToilet, setJsonToilet] = useState(null);
  //JSON UPRN file for MapWeb -- null request
  const[UPRNDataAQ, setUPRNDataAQ] = useState(null);
  //JSON UPRN file for MapWeb
  const[jsonUPRN, setJsonUPRN] = useState(null);

  useEffect(async () => {

    const responseTree = await fetch("/spotify/mapbox_tree");
    const jsonTree = await responseTree.json();
    setJsonTree(jsonTree)

    const responseToilet = await fetch("/spotify/mapbox_toilets");
    const jsonToilet = await responseToilet.json();
    setJsonToilet(jsonToilet)

    const responseUPRN = await fetch("/spotify/mapbox_UPRN");
    const jsonUPRN = await responseUPRN.json();
    setJsonUPRN(jsonUPRN)

}, []);

  const geolocateControlStyle= {
    right: 50,
    top: 10
  };

  const navControlStyle= {
    right: 10,
    top: 10
  };

  return (
    <ReactMapGL
      {...viewport }
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
      mapStyle= "mapbox://styles/diegoguayas/ckmobq6tb44op17pjfanaeljt"

      //Drag the map and zoom it
      onViewportChange={ 
        viewport => {
        setViewport(viewport);
      }} 
      >

      <GeolocateControl
        style={geolocateControlStyle}
        positionOptions={{enableHighAccuracy: true}}
        trackUserLocation={true}
        auto
      />

      <NavigationControl style={navControlStyle} />

      {jsonTree != null && jsonTree.features.map( tree => (
        <Marker key={tree.id} 
        longitude={tree.geometry.coordinates[0]}
        latitude={tree.geometry.coordinates[1]}>

      <Button
        size="small"
        color="primary"
        startIcon={viewport.zoom >= 15 ? (
          <Avatar
            src={treeSVG}
            className={classes.small}
            alt="Tree_Image"
          />) : (
          <Avatar
            src={emptySVG}
            className={classes.small}
            alt="No Image"
          />
          )       
        }
        onClick={(e) => {
 
          e.preventDefault();
          setTreeDataAQ(tree);
        }}
      >
      </Button>
        </Marker>
      ))}

      {jsonToilet != null && jsonToilet.features.map( toilet => (
        <Marker key={toilet.id} 
        longitude={toilet.geometry.coordinates[0]}
        latitude={toilet.geometry.coordinates[1]}>

      <Button
        size="small"
        color="primary"
        startIcon={viewport.zoom >= 15 ? (
          <Avatar
            src={toiletSVG}
            alt="Toilet_Image"
          />) : (
          <Avatar
            src={emptySVG}
            className={classes.small}
          />
          )       
        }
        onClick={(e) => {

          e.preventDefault();
          setPublicToiletAQ(toilet);
        }}
      >
      </Button>
        </Marker>
      ))}

      {jsonUPRN != null && jsonUPRN.features.map( UPRN => (
        <Marker key={UPRN.id} 
        longitude={UPRN.geometry.coordinates[0]}
        latitude={UPRN.geometry.coordinates[1]}>

      <Button
        size="small"
        color="primary"
        startIcon={viewport.zoom >= 17 ? (
          <Avatar
            src={markerSVG}
            className={classes.small}
            alt="UPRN_Points"
          />) : (
          <Avatar
            src={emptySVG}
            className={classes.small}
            alt="No Image"
          />
          )       
        }
        onClick={(e) => {
 
          e.preventDefault();
          setUPRNDataAQ(UPRN);
        }}
      >
      </Button>
        </Marker>
      ))}

      <Button variant="contained" color="primary" to="/" component={Link}>
        Go Back
            </Button>

      {treeDataAQ ? (
        <Popup className="tooltip" 
        latitude={treeDataAQ.geometry.coordinates[1]} 
        longitude={treeDataAQ.geometry.coordinates[0]}
        onClose={() => {
          setTreeDataAQ(null);
        }}
        >
        <div>
        <h2>Tree Type: {treeDataAQ.properties.TREETYPE}</h2>
        <p>Description: {treeDataAQ.properties.DESCRIPT}</p>
        </div>
        </Popup>
      ) : null}

      {publicToiletAQ ? (
        <Popup 
        latitude={publicToiletAQ.geometry.coordinates[1]} 
        longitude={publicToiletAQ.geometry.coordinates[0]}
        onClose={() => {
          setPublicToiletAQ(null);
        }}
        >
          <div>
          <h2>{publicToiletAQ.properties.ServiceTypeLabel}</h2>
          <p>Location: {publicToiletAQ.properties.LocationText}</p>
          <p>Street Name: {publicToiletAQ.properties.StreetAddress}</p>
          </div>
        </Popup>
      ) : null}

      {UPRNDataAQ ? (
        <Popup 
        latitude={UPRNDataAQ.geometry.coordinates[1]} 
        longitude={UPRNDataAQ.geometry.coordinates[0]}
        onClose={() => {
          setUPRNDataAQ(null);
        }}
        >
        <div>
        <h2>UPRN: {UPRNDataAQ.properties.UPRN}</h2>
        <p>X_COORDINATE: {UPRNDataAQ.properties.X_COORDINATE}</p>
        <p>Y_COORDINATE: {UPRNDataAQ.properties.Y_COORDINATE}</p>
        </div>
        </Popup>
      ) : null}

    </ReactMapGL>
  );
}

