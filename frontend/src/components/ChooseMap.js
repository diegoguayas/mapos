import React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';

//s
const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}));

export default function ChooseMap() {
  const classes = useStyles();

  return (
      <div className="App">
        <div className={classes.heroContent}>
          <Container maxWidth="sm">
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
              MapOS
            </Typography>
            <Typography variant="h6" align="justify" color="textSecondary" paragraph>
            This web application will make use of Ordnance Survey (OS) maps and tools to display information
              and prove that it is possible to break the consistent use of Google Maps. Ordnance Survey
              are in charge of creating, maintaining and distributing detailed location information for Great Britain.
              They record and keep 500 million geospatial features in the OS master map up-to-date.
            </Typography>
          </Container>
        </div>
        <Container>
          
          <Grid container spacing={4}>
          <Grid item key={classes.card} xs={6} sm={3} md={3}>

                  <Card className={classes.card}>
                    <CardMedia
                      className={classes.cardMedia}
                      image={DemoJPG}
                      title="Demo Map"
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant="h5" component="h2">
                      Demo Ordnance Survey Map
                      </Typography>
                      <Typography>
                      Demo that shows capabilities of OS maps in contrast to a regular online maps
                      </Typography>
                    </CardContent>
                    <CardActions>
                    <Button size="small" variant="contained" color="primary" to="/DemoMap" component={Link}>
                        View Map
                      </Button>
                    </CardActions>
                  </Card>
         </Grid>

        <Grid item key={classes.card} xs={6} sm={3} md={3}>

        <Card className={classes.card}>
          <CardMedia
            className={classes.cardMedia}
            image={InfoMapa}
            title="Information Map"
          />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
            Information Map
            </Typography>
            <Typography>
            OS map with information of Manchester including a combination of external data taken from
            the Manchester City Council Open Data and OS data. 
            </Typography>
          </CardContent>
          <CardActions>
          <Button size="small" variant="contained" color="primary" to="/InfoMap" component={Link}>
            View Map
          </Button>
          </CardActions>
        </Card>
        </Grid>

         <Grid item key={classes.card} xs={6} sm={3} md={3}>

        <Card className={classes.card}>
          <CardMedia
            className={classes.cardMedia}
            image={MapN}
            title="OS Map"
          />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
            Ordnance Survey Map
            </Typography>
            <Typography>
            OS map with similar attributes to Google Maps. This map has road networks, buildings, natural features, points of interest and more.
            </Typography>
          </CardContent>
          <CardActions>
          <Button size="small" variant="contained" color="primary" to="/OSMap" component={Link}>
            View Map
            </Button>
          </CardActions>
        </Card>
        </Grid>

        <Grid item key={classes.card} xs={6} sm={3} md={3}>

        <Card className={classes.card}>
          <CardMedia
            className={classes.cardMedia}
            image="https://www.ordnancesurvey.co.uk/image-library/products/open-rivers-detail.x8c811353.jpg?q=100&w=1000"
            title="OS Rivers data"
          />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
            Ordnance Survey Open Rivers
            </Typography>
            <Typography>
            OS Open Rivers GIS data contains over 144,000 km of water bodies and watercourses map data.
            These include freshwater rivers, tidal estuaries and canals. 
            </Typography>
          </CardContent>
          <CardActions>
          <Button size="small" variant="contained" color="primary" to="/OSRivers" component={Link}>
          View Map
          </Button>
          </CardActions>
        </Card>
        </Grid>




         </Grid>


        </Container>
      </div>
  );



}