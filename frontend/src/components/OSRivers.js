import * as React from 'react';
import { useState, useEffect } from 'react';
import ReactMapGL, {GeolocateControl, NavigationControl} from 'react-map-gl';
import { Link} from "react-router-dom";
import { Button } from "@material-ui/core";


export default function OSRivers() {
  const [viewport, setViewport] = useState({
    width: "100vw",
    height: "100vh",
    latitude:  53.483959,
    longitude: -2.244644,
    zoom: 9
  });
  
const [mapStyle, setMapStyle] = useState('');

  const geolocateControlStyle= {
    right: 50,
    top: 10

  };

  const navControlStyle= {
    right: 10,
    top: 10
  };

  return (
    <ReactMapGL
      {...viewport }
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
      mapStyle= "mapbox://styles/diegoguayas/ckmpchibs004q17rxn0tf4cil"

      //Drag the map and zoom it
      onViewportChange={ 
        viewport => {
        setViewport(viewport);
      }} 
      >

      <GeolocateControl
        style={geolocateControlStyle}
        fitBoundsOptions={{maxZoom: 10}}
        positionOptions={{enableHighAccuracy: true}}
        trackUserLocation={true}
        auto
      />
      
      <NavigationControl style={navControlStyle} />


      <Button variant="contained" color="primary" to="/" component={Link}>
              Go Back
            </Button>

    </ReactMapGL>
  );
}

