from django.urls import path
from .views import *

urlpatterns = [
    path('mapbox_tree',DatasetQuery_Tree.as_view()),
    path('mapbox_toilets',DatasetQuery_Toilet.as_view()),
    path('mapbox_UPRN',DatasetQuery_UPRN.as_view()),
]