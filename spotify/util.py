from requests import get

BASE_URL_TREE = "https://api.mapbox.com/datasets/v1/diegoguayas/"

def datasetInfo(dataset, token):
        response = get(BASE_URL_TREE + dataset + "/features?access_token=" + token)
        return response.json()




