from rest_framework.response import Response
from .credentials import ACCESS_TOKEN_MAPBOX
from rest_framework.views import APIView
from rest_framework import status

from .util import *

class DatasetQuery_Tree(APIView):
    def get(self, request, format=None):
        dataset_ID = "ckmlkon9l4erm21qn10dt37h1"
        response = datasetInfo(dataset_ID, ACCESS_TOKEN_MAPBOX)
        return Response(response, status=status.HTTP_200_OK)

class DatasetQuery_Toilet(APIView):
    def get(self, request, format=None):
        dataset_ID = "ckmociqbd00sc28mmiqxw6o20"
        response = datasetInfo(dataset_ID, ACCESS_TOKEN_MAPBOX)
        return Response(response, status=status.HTTP_200_OK)

class DatasetQuery_UPRN(APIView):
    def get(self, request, format=None):
        dataset_ID = "ckn9tfl8z363u21pbtya4bil0"
        response = datasetInfo(dataset_ID, ACCESS_TOKEN_MAPBOX)
        return Response(response, status=status.HTTP_200_OK)

